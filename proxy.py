import logging
import signal
import socket
import socketserver
import sys
from http import HTTPStatus

from config import HOST, PORT, PROXY_HOST, PROXY_PORT

logger = logging.getLogger('wolverine_proxy')
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
logger.addHandler(handler)

LINE_SEPARATOR = '\r\n'
SPLITER = bytes(LINE_SEPARATOR + LINE_SEPARATOR, 'utf-8')


class Handler(socketserver.BaseRequestHandler):

    def send_response(self, code=HTTPStatus.OK, content=''):
        response = ('HTTP/1.1 {code.value} {code.phrase}'.format(code=code),
                    'Access-Control-Allow-Origin: *',
                    'Allow: OPTIONS, POST',
                    '',  # Required for a proper HTTP response.
                    content)
        self.request.send(bytes(LINE_SEPARATOR.join(response), 'utf-8'))

    def handle(self):
        data = self.request.recv(1024)
        if data.startswith(b'OPTIONS'):
            self.send_response()
        elif data.startswith(b'POST'):
            try:
                _, data = data.split(SPLITER, 1)
            except ValueError:
                logger.error('Unable to parse "%s"', data)
                self.send_response(code=HTTPStatus.BAD_REQUEST,
                                   content='Invalid HTTP request.')
            if data:
                logger.debug(data)
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                sock.sendto(data, (HOST, PORT))
                response, addr = sock.recvfrom(1024)
                self.send_response(code=HTTPStatus.ACCEPTED,
                                   content=response.decode())
            else:
                self.send_response(code=HTTPStatus.BAD_REQUEST,
                                   content='Empty request.')
        else:
            self.send_response(code=HTTPStatus.METHOD_NOT_ALLOWED)


if __name__ == '__main__':

    def shutdown(*args):
        # signal will pass args, but we don't use them.
        print('Terminating')
        server.server_close()
        sys.exit(0)

    socketserver.TCPServer.allow_reuse_address = True
    server = socketserver.TCPServer((PROXY_HOST, PROXY_PORT), Handler)
    signal.signal(signal.SIGTERM, shutdown)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    finally:
        shutdown()
