import socket
import socketserver
from multiprocessing import Process

import pytest
from config import HOST, PORT
from connections import redis_connection
from server import Handler


@pytest.fixture(scope='module')
def client():
    return socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


@pytest.fixture(scope='module')
def server():
    s = socketserver.UDPServer((HOST, PORT), Handler)
    p = Process(target=s.serve_forever)
    p.start()
    yield s.server_address
    p.terminate()


def pytest_runtest_teardown():
    redis_connection.zremrangebyscore('queue', '-inf', 'inf')
