import hashlib
import logging
import socketserver
from datetime import datetime, timezone

from config import HOST, PORT
from connections import redis_connection

logger = logging.getLogger('wolverine')
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
logger.addHandler(handler)


class Handler(socketserver.BaseRequestHandler):

    def handle(self):
        data = self.request[0].strip()
        logger.info(data)
        socket = self.request[1]
        score = datetime.now(timezone.utc).timestamp()
        redis_connection.zadd('queue', score, data)
        key = hashlib.md5(data).hexdigest()
        url = 'https://drone.api.gouv.fr/data/report/{}'.format(key)
        content = 'Accepted, optional error report: {}'.format(url)
        socket.sendto(bytes(content, 'utf-8'), self.client_address)


if __name__ == '__main__':
    # Use `with` if Python 3.6 support only.
    logger.info('Server starting')
    server = socketserver.UDPServer((HOST, PORT), Handler)
    server.serve_forever()
