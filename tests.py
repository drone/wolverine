import json

from connections import redis_connection


def test_response_ok(client, server):
    client.sendto(b'foo', server)
    result = client.recv(1024)
    assert (b'Accepted, optional error report: '
            b'https://drone.api.gouv.fr/data/report/') in result


def test_redis_zset(client, server):
    client.sendto(b'foo', server)
    client.recv(1024)  # Wait for response to be processed.
    assert redis_connection.zrange('queue', 0, -1) == [b'foo']


def test_payload_is_striped(client, server):
    client.sendto(b' foo\n', server)
    client.recv(1024)  # Wait for response to be processed.
    assert redis_connection.zrange('queue', 0, -1) == [b'foo']


def test_real_payload(client, server):
    payload = {
        'registration': 'ABCDEFG',
        'time': '2017-06-14T13:11:51+00:00',
        'coordinates': [48.7517, 2.7905, 131]
    }
    encoding = 'utf-8'
    data = bytes(json.dumps(payload), encoding=encoding)
    client.sendto(data, server)
    client.recv(1024)  # Wait for response to be processed.
    data2 = redis_connection.zrange('queue', 0, -1)
    assert len(data2) == 1
    payload2 = json.loads(data2[0].decode(encoding))
    assert payload == payload2
