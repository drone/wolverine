import os

HOST = os.environ.get('HOST', 'localhost')
PORT = 5005
PROXY_HOST = os.environ.get('PROXY_HOST', 'localhost')
PROXY_PORT = 5006
REDIS_DB = 0
REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
REDIS_PORT = 6379
