# Wolverine

Python service that accepts UDP packets from drones and
stores raw payloads in Redis.
See the [meta respository](https://framagit.org/drone/meta) to undestand
the big picture.

A HTTP > UDP proxy is also available.


## Constraints

There are about [500k drone on the French territory](http://drones.blog.lemonde.fr/2016/03/21/en-france-les-ventes-de-drones-ont-triple-en-2015/) as of mid-2017.

[Ten percent](http://www.lemonde.fr/la-foire-du-drone/article/2017/03/17/le-canada-encadre-strictement-les-drones-de-loisirs_5096210_5037916.html) of which are above the [800g limit](http://drones.blog.lemonde.fr/2016/09/28/drones-de-loisir-la-reglementation-prend-forme/) (exact regulated weight limit not yet known but should be around that limit).

We have to handle at least 5k requests/second and at best 50k requests/second.


## Packets specification

```
{
    "registration": "registration number",
    "time": "ISO 8601:2004",
    "coordinates": [lon, lat, alt]
}
```

For instance:

```
{
    "registration": "ABCDEFG",
    "time": "2017-06-14T13:11:51+00:00",
    "coordinates": [2.7905, 48.7517, 131]
}
```

## Using HTTP

HTTP calls should pass through the proxy (localhost:5006) that redirect to the
Wolverine UDP server (localhost:5005).

Run the proxy `python proxy.py` in parallel to the server `python server.py`.


## Testing

Launch a Redis server and adapt the config file consequently.

### Using the client

```
python client.py foo
```

### Using nc

```
nc -u 127.0.0.1 5005
```

### Using iperf

```
iperf --port 5005 --udp --client 127.0.0.1 --parallel 2 --bandwidth 2M
```

### Using pytest

```
py.test tests.py
```
