import argparse
import hashlib
import json
import random
import socket
import sys
import time
from datetime import datetime, timezone

from config import HOST, PORT

parser = argparse.ArgumentParser()
parser.add_argument('-H', '--host', default=HOST)
parser.add_argument('-P', '--payload')
parser.add_argument('-B', '--bbox', nargs=4,
                    default=[-4.99, 41.34, 9.69, 51.04])  # France metro.
parser.add_argument('-C', '--count', default=1, type=int)
parser.add_argument('-T', '--tracks', default=1, type=int)
args = parser.parse_args()
data = args.payload


def send_data(data, verbose=True):
    bytes_data = bytes(data, 'utf-8')
    error_hash = hashlib.md5(bytes_data).hexdigest()
    if verbose:
        print('Sending: {} to {}'.format(data, args.host))
        print('Error hash: {} (useful to debug)'.format(error_hash))

    # SOCK_DGRAM is the socket type to use for UDP sockets
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # As you can see, there is no connect() call; UDP has no connections.
    # Instead, data is directly sent to the recipient via sendto().
    sock.sendto(bytes_data, (args.host, PORT))
    received = str(sock.recv(1024), 'utf-8')

    if verbose:
        print('Valid connection to {}'.format(args.host))
        print('Received: {} from {}'.format(received, args.host))


# Payload takes precedence over other parameters.
if args.payload:
    send_data(args.payload)
    sys.exit()

positions = {}
hostname = socket.gethostname()
for t in range(args.tracks):
    for n in range(args.count):
        registration = f'{hostname}{n}'
        timestamp = datetime.now(timezone.utc).isoformat()
        elevation = random.randint(0, 150)
        if positions.get(registration):
            # On metropolitan France area, one degree is around 80 km,
            # the given range allows a drone to fly at 100 km/h max.
            lat = positions[registration][0] + random.uniform(-0.001, .001)
            lon = positions[registration][1] + random.uniform(-0.001, .001)
        else:
            lat = random.uniform(args.bbox[0], args.bbox[2])
            lon = random.uniform(args.bbox[1], args.bbox[3])
        data = {
            'registration': registration,
            'time': timestamp,
            'coordinates': [lat, lon, elevation]
        }
        positions[registration] = [lat, lon]
        send_data(json.dumps(data), verbose=False)

    time.sleep(3)
